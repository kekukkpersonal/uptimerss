(function () {
    'use strict';

    var FEED = "https://flipboard.com/@raimoseero/feed-nii8kd0sz?rss"; 
    var MERCURY_URL = 'https://mercury.postlight.com/parser?url=';
    var MERCURY_API_KEY = 'RY9Iq28lN6Yn9lrNLTe4qmlz0WD0UgJyEwRfWDcw'; 

    
    CheckSuccess(FEED);
    LoadFeed(FEED);
    ModalClosed('#myModal');

    function LoadFeed(url){
        $.get(url, function(data) {
            var $xml = $(data);
            $xml.find("item").each(function(i) {
                i++;

                // create item
                var $this = $(this),
                    item = {
                        title: $this.find("title").text(),
                        link: $this.find("link").text(),
                        pubDate: $this.find("pubDate").text(),
                        author: $this.find("author").text(),
                        description: $this.find("description").text(),
                        img: $this.find('media\\:content, content').attr('url')
                }     
    
                // append item data into elements
                $('#feed').append(
                    $('<div/>').addClass("col-md-4 clearfix")
                        .append($('<img/>').attr("src",item.img))
                        .append($('<h4/>').text(item.title))
                        .append($('<p/>').text(item.pubDate))
                        .append($('<p/>').text(item.author).addClass("author"))
                        .append($('<p/>').text(item.description).addClass("description"))
                        .append(
                            $('<p/>').append(
                                // Trigger the modal with a button
                                $("<button onclick='ModalAction(this)' link='" + item.link + "' type='button' data-toggle='modal' data-target='#myModal'/>")
                                    .addClass("btn btn-default")
                                    .text("Read full article")
                            )
                        )
                );
                
                // adds space after third item
                if(i % 3 === 0){
                    $('#feed').append(
                        $('<div/>').addClass("spacer")
                    );
                }

            });
        });
    }

    function CheckSuccess(url){
        $.ajax({
            type: 'GET',
            dataType: 'xml',
            url: url,
            timeout: 5000,
            success: function(data, textStatus ){
               console.log('Request successful!');
            },
            error: function(xhr, textStatus, errorThrown){
               alert('Something went wrong. Look into console.');
            }
          });
    }

    function LoadArticle(articleUrl){
        return Promise.resolve($.ajax({
            type: 'GET',
            url: MERCURY_URL + articleUrl,
            headers: {
                "x-api-key": MERCURY_API_KEY
            }
        }));
    }

    // gobal function
    window.ModalAction = function(data){
        
        var link = data.getAttribute("link");
        var asset = LoadArticle(link);

        asset.then(function(result) {
            OnSuccess(result);
        }, function(err) {
            OnError(err);
        });
    }

    // do something when modal is closed
    function ModalClosed(id){
        $(id).on('hidden.bs.modal', function () {
            $('#myModal .modal-title').text('Loading title...');
            $('#myModal .modal-body').text('Loading content...');
        })
    }
    
    function OnSuccess(result){
        $('#myModal .modal-title').text('').append().text(result.title);
        $('#myModal .modal-body').text('').append($(result.content));
    }

    function OnError(err){
        var errMessage1 = err.responseJSON.Message;
        var errMessage2 = err.responseJSON.message;

        $('#myModal .modal-title').text('Error');
        if (errMessage2 == null){
            $('#myModal .modal-body').text(errMessage1);
        } else{
            $('#myModal .modal-body').text(errMessage2);
        }
    }

})();